################################################################################
# Package: JetTagCalibration
################################################################################

# Declare the package name:
atlas_subdir( JetTagCalibration )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          Control/StoreGate
                          GaudiKernel
                          PRIVATE
#                          PhysicsAnalysis/JetTagging/JetTagTools
                          Database/APR/FileCatalog
                          Database/AthenaPOOL/AthenaPoolUtilities
                          Database/AthenaPOOL/PoolSvc
                          DetectorDescription/DetDescrCond/DetDescrCondTools )

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )
find_package( Eigen )

# Component(s) in the package:
atlas_add_library( JetTagCalibrationLib
                   src/*.cxx
                   PUBLIC_HEADERS JetTagCalibration
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES} ${EIGEN_LIBRARIES} AthenaBaseComps GaudiKernel StoreGateLib SGtests FileCatalog
                   PRIVATE_LINK_LIBRARIES AthenaPoolUtilities )

atlas_add_component( JetTagCalibration
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaBaseComps StoreGateLib SGtests GaudiKernel FileCatalog AthenaPoolUtilities JetTagCalibrationLib )

# Install files from the package:
atlas_install_joboptions( share/*.py )
atlas_install_runtime( share/*.db )

