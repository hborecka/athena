/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PIXELCONDITIONSSERVICES_IPIXELDCSSVC_H
#define PIXELCONDITIONSSERVICES_IPIXELDCSSVC_H


#include "PixelConditionsTools/IPixelDCSSvc.h"


#endif // not PIXELCONDITIONSSERVICES_IPIXELDCSSVC_H
